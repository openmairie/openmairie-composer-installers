# openmairie-composer-installers

Composer plugin to define openMairie types installers.


## openmairie-layout

To define a package as an openMairie layout package, add the following to your package's composer file:

```json
"name": "openmairie/openmairie-layout-LAYOUT_NAME",
"type": "openmairie-layout",
"require": {
	"openmairie/openmairie-composer-installers": "^1.0"
}
```

This package will install a `openmairie-layout` type package in the `om-layout/LAYOUT_NAME/` directory. 

For now, the package name must start with `openmairie/openmairie-layout-`.


