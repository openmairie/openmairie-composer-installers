<?php

namespace openMairie\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class LayoutInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $prefix = substr($package->getPrettyName(), 0, 29);
        if ('openmairie/openmairie-layout-' !== $prefix) {
            throw new \InvalidArgumentException(
                'Unable to install layout, openmairie layout '
                .'should always start their package name with '
                .'"openmairie/openmairie-layout-"'
            );
        }

        return 'om-layout/'.substr($package->getPrettyName(), 29);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'openmairie-layout' === $packageType;
    }
}
