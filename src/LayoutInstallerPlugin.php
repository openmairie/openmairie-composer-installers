<?php

namespace openMairie\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class LayoutInstallerPlugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new LayoutInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}
